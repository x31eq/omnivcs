Version Control Scripts
=======================

These are scripts to wrap version control commands in
reasonably consistent way that I like.  They focus on simple
behavior with some non-standard options supplied.  Sometimes
command line arguments are passed along but if you want the
original defaults or more complicated behavior you can use
the original commands.  Extract and add to your path.

Supported systems are Git, Mercurial, and Subversion.  You
can get a fair way with Git and Mercurial both managing the
same folder.  There is some support for git-svn.

I started with high ideals where everything should work with a standard UNIX shell (tested
with dash) and a reasonable UNIX environment.  Each script
was independent so you could safely rename them except mergeto
which calls switch and mergein.  The names are partly chosen
to avoid clashes with existing commands.

Then I decided mergeto was mostly duplicating mergein and switch.
So now mergeto depends on mergein and switch.

Then I decided it would be nice to do a rebase in Subversion.
So now there's a Python file to do that.
For a while it was in a separate branch, but not any more.

The repository is online as git@bitbucket.org:x31eq/omnivcs.git
I keep a copy in Mercurial as well if anybody wants that.

To see what the commands do, look at the source.  Some
pointers though:

add      -> add
changes  -> log
commit   -> commit -m "$*"
diffs    -> diff
mergein  -> merge && rm branch
mergeto  -> switch && mergein
pull     -> pull --rebase
push     -> push
rebase   -> rebase
rollback -> undo commit
switch   -> checkout
