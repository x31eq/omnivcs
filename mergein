#!/bin/sh

# Merge from the given branch,
# making a single change,
# and close the donor branch.

mute() {
    $@ >/dev/null 2>/dev/null
}

mute_err() {
    $@ 2>/dev/null
}

err_exit() {
    echo $@ >&2
    exit 1
}

svnchanged=$(mute_err svn status -q)
hgchanged=$(mute_err hg status -mardn)
gitchanged=$(mute_err git status -s -uno)
[ -n "$gitchanged$svnchanged$hgchanged" ] &&
    err_exit Uncommitted changes in current branch.

origin="$1"
[ -z "$1" ] && err_exit Supply the branch name as the first argument.
shift
commit_message="${*:-Merged branch $origin}"

set -e

if mute git status
then
    hash=$(git log --pretty=%H "$origin~" -1)
    git merge --squash "$origin"
    git commit -m "$commit_message

absent parent $hash"
    git branch -D "$origin"
fi

if mute hg status
then
    hg update -C
    hg merge "$origin"
    hg commit -m "$commit_message"
fi

if mute svn info
then
    svn merge --reintegrate "^/branches/$origin"
    svn commit --force-log -m "$commit_message"
    svn rm "^/branches/$origin" -m "Deleting merged branch"
fi
