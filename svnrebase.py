#!/usr/bin/env python

import sys, os, re, subprocess, tempfile
from xml.etree import ElementTree

BRANCH_EXPR = '^/trunk|/branches/[^/]+'


class Rebaser(object):
    def __init__(self, base, args=()):
        """
        base: the branch being rebased
        args: arguments to feed to the merge command
        """
        self.base = base
        self.args = args
        self.current_head = os.path.split(os.getcwd())[-1]

    def rebase(self, newbase=None):
        destination, initial_revision = get_relative_path()

        changesets = list(
                self.get_changesets(destination, initial_revision))
        changesets.reverse()

        upstream, message = changesets[0]
        if newbase is None:
            newbase = re.search(BRANCH_EXPR, upstream).group(0)
        else:
            upstream = newbase + re.sub(BRANCH_EXPR, '', newbase)
        oldbase = re.search(BRANCH_EXPR, destination).group(0)
        changesets[0] = [
            ['svn', 'rm', '^' + oldbase,
                '-m', 'Rebasing %s on %s' % (destination, newbase),
            ],
            ['svn', 'cp', '^' + newbase, '^' + oldbase,
                '-m', message],
            ['svn', 'switch', '^' + destination],
        ]

        for changeset in changesets:
            for command in changeset:
                while subprocess.call(command):
                    print("Command failed:")
                    print(' '.join(command))
                    choice = raw_input("Retry? ")
                    if choice[0] in 'nN':
                        break

    def get_changesets(self, head, initial_revision):
        """
        Generate the changesets, newest first,
        from the head location to the branch point from self.base.
        Append args to the merges.
        """
        upstream = self.base
        svn_log = subprocess.Popen(
                ['svn', 'log',
                    '--stop-on-copy', '-v', '--xml',
                    '^%s@%s' % (head, initial_revision)],
                stdout=subprocess.PIPE,
        )
        text_log = svn_log.stdout.read()
        xml_log = ElementTree.fromstring(text_log)
        for entry in xml_log.findall('logentry'):
            revision = entry.attrib['revision']
            message = entry.find('msg').text
            if is_branch_point(entry, head):
                path = entry.find('paths/path')
                base = path.attrib['copyfrom-path']
                if upstream is None:
                    # This is the latest branch point
                    # where the base wasn't specified.
                    # Give the message as a special changeset.
                    # It will always be the last yielded.
                    yield (base, message)
                elif base.startswith(upstream):
                    # The branch was created from a known base.
                    # Correct so that only the subfolder is copied.
                    working = re.sub(BRANCH_EXPR, '', head)
                    if working.startswith('/'):
                        base = upstream + working
                    # Give the message as a special changeset.
                    # It will always be the last yielded.
                    yield (base, message)
                else:
                    # The branch was copied, so carry on down.
                    working = re.sub(BRANCH_EXPR, '', head)
                    if working.startswith('/'):
                        base_branch = re.findall(BRANCH_EXPR, base)[0]
                        base = base_branch + working
                    if 'copyfrom-rev' in path.attrib:
                        copyfrom = path.attrib['copyfrom-rev']
                    else:
                        copyfrom = int(revision) - 1
                    more = self.get_changesets(base, copyfrom)
                    for changeset in more:
                        if len(changeset) > 1:
                            yield changeset
                return
            yield self.merge_changeset(revision, head, message)
        raise Exception("Branch point not found")

    def merge_changeset(self, revision, source, message):
        """Return commands for a changeset to merge"""
        return [
            ['svn', 'merge', '-c', revision,
             '^%s@%s' % (source, revision),
             # Work around svn bug
             # http://svn.haxx.se/dev/archive-2004-10/1485.shtml
             os.path.join('..', self.current_head),
             ] + self.args,
            ['svn', 'commit', '-m', message],
            ['svn', 'update'],
        ]


def get_source_path_args():
    def branch_name(relative_path):
        """Make relative paths relative to /branches"""
        return os.path.join('/branches', relative_path)

    if sys.argv:
        if sys.argv[1].startswith('-'):
            branch = None
            args = sys.argv[1:]
        else:
            branch = branch_name(sys.argv[1])
            args = sys.argv[2:]
        # Find an --onto argument.
        # Other arguments pass through to subversion,
        # so we can't use standard parsers.
        newbase = None
        argiter = iter(args)
        args = []
        for arg in argiter:
            if arg == '--onto':
                newbase = branch_name(argiter.next())
            else:
                args.append(arg)
        return branch, newbase, args
    return None, None, []


def get_relative_path():
    svn_info = subprocess.Popen(
            ['svn', 'info', '--xml'],
            stdout=subprocess.PIPE,
    )
    xml_info = ElementTree.fromstring(svn_info.stdout.read())
    entry = xml_info.find('entry')
    url = entry.find('url').text
    root_url = entry.find('repository/root').text
    assert url.startswith(root_url)
    relative_path = url[len(root_url):]
    if re.search(BRANCH_EXPR, relative_path):
        working = re.sub(BRANCH_EXPR, '', relative_path)
    else:
        raise Exception("Location not branchable.")
    if working:
        sys.stderr.write("Not in branch/trunk root.\n")
        sys.stderr.write("Changes below this folder will be lost.\n")
    return relative_path, entry.attrib['revision']


def is_branch_point(xml_entry, branch):
    """Say if this is the changeset for the branch being copied."""
    if len(xml_entry.findall('paths/path')) == 0:
        return False
    if len(xml_entry.findall('paths/path')) > 2:
        # Two entries for a rename: take the first one
        return False
    path = xml_entry.find('paths/path')
    if path.attrib['kind'] != 'dir':
        return False
    if path.attrib['action'] != 'A':
        return False
    if not branch.startswith(path.text):
        return False
    if 'copyfrom-path' not in path.attrib:
        raise Exception("Branch wasn't branched?")
    return True


if __name__=='__main__':
    branch, newbase, args = get_source_path_args()
    Rebaser(branch, args).rebase(newbase)
